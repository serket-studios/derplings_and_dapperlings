﻿/*
    ------------------ Voronoi Noise -------------------
 
    Voronoi noise seeds an area with a number of center points
    and then creates regions based on the shortest distance to
    each of those points.

    Useful for creating biomes.  Can create different rules
    depending on depth.

    Create the following parameters on the Game Object you wish
    to have generate Voronoi Noise:

    - Width (int, > 0)
    - Height (int, > 0)
    - Seed (int)
    - NumberOfSeeds (int, Range(1,1024))
    - CentroidOffset (int, Range(1,64))
    - Map (Tilemap)
    - Tiles (TileBase[])

    Can be combined with Perlin Noise for greater effect by
    applying a z-value based on the height map data:

    https://imgur.com/gallery/8OfWc

    https://www.youtube.com/watch?v=1qxytunVuqQ

    ----------------------------------------------------
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public static class VoronoiNoise_Old
{
    static List<Vector3Int> centroids = new List<Vector3Int>();
    static List<Vector3Int> centroidsToBeCulled = new List<Vector3Int>();
    static List<int> tileListIndex = new List<int>();

    // Do all the things
    public static void AutoGenerateVoronoiTilemap(int width, int height, int cullingHeight, int seed, int numberOfCentroids, Tilemap tilemap, TileBase[] tiles, int voronoiSeedOffset, bool useRandomSeed)
    {
        Reset(tilemap);

        if (useRandomSeed)
            CreateRandomSeeds(width, height, numberOfCentroids, tilemap, tiles);
        else
            CreateSeeds(width, height, seed, numberOfCentroids, tilemap, tiles, voronoiSeedOffset);

        GenerateMap(width, height, cullingHeight, tilemap, tiles);
    }


    //// Generate map with culling
    //public static void GenerateMapWithCulling(int width, int height, int seed, int numberOfCentroids, Tilemap tilemap, TileBase[] tiles, int voronoiSeedOffset, bool useRandomSeed, int cullingHeight, TileBase cullingTile)
    //{
    //    Reset(tilemap);

    //    if (useRandomSeed)
    //        CreateRandomSeeds(width, height, numberOfCentroids, tilemap, tiles);
    //    else
    //        CreateSeeds(width, height, seed, numberOfCentroids, tilemap, tiles, voronoiSeedOffset);

    //    FlagSeedsForDeletion(cullingHeight, height, tilemap, cullingTile);
    //    GenerateMap(width, height, tilemap, tiles);
    //    //CullRegions(width, height, tilemap, cullingTile);
    //}



    // Create seeds
    public static void CreateSeeds(int width, int height, int seed, int numberOfCentroids, Tilemap tilemap, TileBase[] tiles, int voronoiSeedOffset)
    {
        // Seed
        System.Random prng = new System.Random(seed);


        // Calculate rows and columns
        int rows;
        int columns;

        rows = Mathf.FloorToInt(Mathf.Sqrt(numberOfCentroids));
        while (numberOfCentroids % rows != 0)
            rows -= 1;
        columns = numberOfCentroids / rows;


        // Avoid division by zero
        if (rows < 1)
            rows = 1;
        if (columns < 1)
            columns = 1;
        if (numberOfCentroids < 1)
            numberOfCentroids = 1;
        

        // Loop through all cells
        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                Vector3Int offset = new Vector3Int(0,0,0);

                offset.x = prng.Next(-voronoiSeedOffset, voronoiSeedOffset);
                offset.y = prng.Next(-voronoiSeedOffset, voronoiSeedOffset);

                int posX = x * width / columns + offset.x;
                int posY = y * height / rows + offset.y;

                Vector3Int centroid = new Vector3Int(Mathf.Clamp(posX, 0, width), Mathf.Clamp(posY, 0, height), 0);


                centroids.Add(centroid);
                int tileIndex = prng.Next(0, tiles.Length);
                tileListIndex.Add(tileIndex);

                Vector3Int cell = new Vector3Int(centroid.x, centroid.y, 0);
                TileBase tile = tiles[tileIndex];
                tilemap.SetTile(cell, tile);
            }
        }
    }



    // Create random seeds
    public static void CreateRandomSeeds(int width, int height, int numberOfSeeds, Tilemap tilemap, TileBase[] tiles)
    {
        // Loop through all cells
        for (int i = 0; i < numberOfSeeds; i++)
        {
            Vector3Int centroid = new Vector3Int(Random.Range(0, width - 1), Random.Range(0, height - 1), 0);
            centroids.Add(centroid);

            int randomTileIndex = Random.Range(0, tiles.Length);
            tileListIndex.Add(randomTileIndex);

            Vector3Int cell = new Vector3Int(centroid.x, centroid.y, 0);
            TileBase tile = tiles[randomTileIndex];
            tilemap.SetTile(cell, tile);
        }
    }


    /// <summary>
    /// Marks centroids for regions to be deleted.
    /// </summary>
    /// <param name="cullingHeight"></param>
    /// <param name="height"></param>
    /// <param name="centroids"></param>
    /// <param name="tilemap"></param>
    /// <param name="cullingTile"></param>
    //public static void FlagSeedsForDeletion(int cullingHeight, int height, Tilemap tilemap, TileBase cullingTile)
    //{
    //    for (int i = 0; i < centroids.Count; i++)
    //    {
    //        // Check if above culling height
    //        if (centroids[i].y > height - cullingHeight)
    //        {
    //            centroidsToBeCulled.Add(centroids[i]);
    //            tilemap.SetTile(centroids[i], cullingTile);
    //        }
    //    }
    //}



    // Return closest cell index
    private static int FindClosestCell(Vector3Int cell)
    {
        int closestCellIndex = 0;
        var distance = Vector3Int.Distance(cell, centroids[0]);

        for (int i = 0; i < centroids.Count; i++)
        {
            var tempDistance = Vector3Int.Distance(cell, centroids[i]);
            if (tempDistance < distance)
            {
                distance = tempDistance;
                closestCellIndex = i;
            }
        }

        return closestCellIndex;
    }


    // Generate map
    public static void GenerateMap(int width, int height, int cullingHeight, Tilemap tilemap, TileBase[] tiles)
    {
        // Loop through all cells
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Vector3Int cell = new Vector3Int(x, y, 0);

                // If not one of our seeds
                if (!centroids.Contains(cell))
                {
                    int closestCellIndex = FindClosestCell(cell);

                    Vector3Int pos = new Vector3Int(cell.x, cell.y, 0);

                    TileBase tile = (centroids[closestCellIndex].y > height - cullingHeight) ? null: tiles[tileListIndex[closestCellIndex]];
                    tilemap.SetTile(pos, tile);
                }
            }
        }


        // Cull remaining seeds
        for (int i = 0; i < centroids.Count; i++)
        {
            if (centroids[i].y > height - cullingHeight)
                tilemap.SetTile(centroids[i], null);
        }
    }


    //// Cull regions
    //public static void CullRegions(int width, int height, Tilemap tilemap, TileBase cullingTile)
    //{
    //    for (int x = 0; x < width; x++)
    //    {
    //        for (int y = 0; y < height; y++)
    //        {
    //            Vector3Int pos = new Vector3Int(x, y, 0);
    //            if (tilemap.GetTile(pos) == cullingTile)
    //                tilemap.SetTile(pos, null);
    //        }
    //    }
    //}


    // Reset
    public static void Reset(Tilemap tilemap)
    {
        //TileUtils.ClearMap(tilemap);
        centroids.Clear();
        tileListIndex.Clear();
    }
}
