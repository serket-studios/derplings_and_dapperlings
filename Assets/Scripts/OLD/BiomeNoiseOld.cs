﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

////[RequireComponent(typeof(MeshRenderer))]
public static class BiomeNoiseOld
{
//    // Create
//    public static Biome[,] Create(float[,] voronoi, int width, int height, int borderThickness)
//    {
//        Biome[,] biomeMap = new Biome[width, height];

//        float[,] hybridMap = new float[width, height];
//        Color[] colorArray = new Color[width * height];
//        Color voronoiColor = Color.clear;
//        //Color perlinColor = Color.clear;

//        // Biomes by reverse index
//        List<Biome> biomes = MapGenerator.i.biomeList.GetBiomes();
//        BiomeListSO.SetBiomeValues(biomes);
//        biomes.Reverse();

//        //float threshold = MapGenerator.i.voidThreshold;
//        //Color baseColor = new Color(threshold, threshold, threshold, 1);


//        // Loop through map
//        for (int x = 0; x < width; x++)
//        {
//            for (int y = 0; y < height; y++)
//            {
//                //// Check if in bounds
//                //if(NoiseUtils.InBounds(x, y, width, height, borderThickness))
//                //{
//                // Get voronoi value
//                float v = voronoi[x, y];

//                // Check if in bounds
//                //if (NoiseUtils.InBounds(x, y, borderThickness))
//                //{
//                    // Cycle through biomes
//                    for (int i = 0; i < biomes.Count; i++)
//                    {
//                        // Cull Sky
//                        if (v <= VoronoiNoise.CULLING_VALUE || y > MapGenerator.Height - MapGenerator.i.sheerHeight)
//                        {
//                            BiomeManager.SetBiomeAtCoords(x, y, BiomeType.Sky, biomeMap);
//                            //voronoiColor = Biome.SKY.biomeData.color;
//                            //voronoiColor = Color.clear;
//                        }

//                    //// Return color of closest match
//                    //else 
//                        if (v <= biomes[i].biomeData.value)
//                        {
//                            BiomeManager.SetBiomeAtCoords(x, y, biomes[i].biomeData.type, biomeMap);
//                            //voronoiColor = biomes[i].biomeData.color;
//                        }
//                    }
//                //}


//                //// Border
//                //else //if (!NoiseUtils.InBounds(x, y, width, height, borderThickness))
//                //{
//                //    BiomeManager.SetBiomeAtCoords(x, y, BiomeType.Border, biomeMap);
//                //    colorArray[y * width + x] = Biome.BORDER.biomeData.color;
//                //}


//                ///// Use background terrain tiles instead of colors
//                ///// Use CM's Grid2D tilemaps to create mesh UVs instead
//                ///// Set tile visual based on modulo for x and y
//                ///
//                /// Consider switching to Tilemaps if you can as they're more performant for less effort
//                /// See if you can use CM's Grid with tilemaps, or refactor it.
//                /// 
//                //// Check perlin value
//                //float p = perlin[x, y];
//                //perlinColor = p < threshold ? Color.clear : Color.Lerp(baseColor, Color.white, p);

//                //// Compare to tiles in biome
//                //Color c = perlinColor * voronoiColor;
//                //colorArray[y * width + x] = c;


//                //// List of tiles by reverse index, compare values.
//                //// If below culling threshhold, set void.
//                /// Use CM's Grid2D Tilemaps to create mesh UVs instead
//                /// Populate with terrain tiles
//                /// Set tile visual based on modulo for x and y
//                /// 
//                /// 
//                /// Create seams between biomes
//                /// Create liquid and gas simulations, light simulations
//                /// Light value will lerp color between black and white based on amount of light
//                //}
//            }
//        }


//        // Assign all neighbors
//        //BiomeManager.AssignAllNeighbors(biomeMap);
//        //BiomeManager.MarkBiomeSeams(biomeMap);
//        //BiomeManager.MarkAsBorder(biomeMap);

//        return biomeMap;
//    }
}
