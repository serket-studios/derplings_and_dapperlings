﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Map;

public static class TileNoise
{
    public static Tile[,] Create(float[,] perlin, int width, int height)
    {
        Biome[,] biomeMap = MapGenerator.i.GetBiomeMap();
        Tile[,] tileMap = new Tile[width, height];

        // Get tile list
        List<Tile> tiles = new List<Tile>();

        // Loop through cells
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                // Check biome at cell
                Biome biome = biomeMap[x, y];

                tiles = biome.TerrainTiles;
                biome.SetTileValues();
                tiles.Reverse();

                // Get perlin height
                float p = perlin[x, y];

                // Set tile based on value
                for (int i = 0; i < tiles.Count; i++)
                {
                    // Return closest match
                    if (p <= tiles[i].Value)
                    {
                        // Set tile
                        tileMap[x, y] = tiles[i];
                    }

                    if (p <= biome.biomeData.voidCutoff)
                    {
                        tileMap[x, y] = Tile.VOID;
                    }
                }
            }
        }

        return tileMap;
    }
}
