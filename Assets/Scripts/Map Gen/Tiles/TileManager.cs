﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Map;

public static class TileManager
{
    // GET AND SET TILE AT SPECIFIC COORDINATES


    // Set Static Tiles
    public static void SetStaticTiles()
    {
        SetStaticTerrainTiles();
        SetStaticTileData();
    }


    // Set Static Tile Data
    public static void SetStaticTileData()
    {
        List<TileData> tileDataList = MapModes.i.ALL_TERRAIN_TILES_LIST.tileDataList;

        TileData.ADAMANTINE     = tileDataList[0];
        TileData.ALUMINIUM      = tileDataList[1];
        TileData.AMBER          = tileDataList[2];
        TileData.AMETHYST       = tileDataList[3];
        TileData.AMPHIBOLITE    = tileDataList[4];
        TileData.AMYGDALITE     = tileDataList[5];

        TileData.BASALT         = tileDataList[6];
        TileData.BISMUTH        = tileDataList[7];
        TileData.BLOODSTONE     = tileDataList[8];
        TileData.BRIMSTONE      = tileDataList[9];

        TileData.CLAY           = tileDataList[10];
        TileData.COAL           = tileDataList[11];
        TileData.COPPER         = tileDataList[12];
        TileData.DIAMOND        = tileDataList[13];
        TileData.DIRT           = tileDataList[14];
        TileData.EMERALD        = tileDataList[15];

        TileData.FELDSPAR       = tileDataList[16];
        TileData.FIRESTONE      = tileDataList[17];
        TileData.FOSSIL         = tileDataList[18];
        TileData.GLOWSTONE      = tileDataList[19];
        TileData.GOLD           = tileDataList[20];
        TileData.GRANITE        = tileDataList[21];

        TileData.ICE            = tileDataList[22];
        TileData.IRON           = tileDataList[23];
        TileData.JADE           = tileDataList[24];
        TileData.LEAD           = tileDataList[25];
        TileData.LIMESTONE      = tileDataList[26];

        TileData.MAGICITE       = tileDataList[27];
        TileData.MARBLE         = tileDataList[28];
        TileData.MUD            = tileDataList[29];
        TileData.MYTHRIL        = tileDataList[30];
        TileData.NETHERAK       = tileDataList[31];

        TileData.OBSIDIAN       = tileDataList[32];
        TileData.QUARTZ         = tileDataList[33];
        TileData.RUBY           = tileDataList[34];

        TileData.SAND           = tileDataList[35];
        TileData.SANDSTONE      = tileDataList[36];
        TileData.SAPPHIRE       = tileDataList[37];
        TileData.SCALE          = tileDataList[38];
        TileData.SHALE          = tileDataList[39];
        TileData.SILVER         = tileDataList[40];
        TileData.SLATE          = tileDataList[41];
        TileData.SLIME          = tileDataList[42];
        TileData.SNOW           = tileDataList[43];
        TileData.STEEL          = tileDataList[44];

        TileData.TITANIUM       = tileDataList[45];
        TileData.TUNGSTEN       = tileDataList[46];
    }


    // Set Static Terrain Tiles
    public static void SetStaticTerrainTiles()
    {
        Tile.ADAMANTINE         = new Tile(TerrainType.Adamantine);
        Tile.ALUMINIUM          = new Tile(TerrainType.Aluminium);
        Tile.AMBER              = new Tile(TerrainType.Amber);
        Tile.AMETHYST           = new Tile(TerrainType.Amethyst);
        Tile.AMPHIBOLITE        = new Tile(TerrainType.Amphibolite);
        Tile.AMYGDALITE         = new Tile(TerrainType.Amygdalite);

        Tile.BASALT             = new Tile(TerrainType.Basalt);
        Tile.BISMUTH            = new Tile(TerrainType.Bismuth);
        Tile.BLOODSTONE         = new Tile(TerrainType.Bloodstone);
        Tile.BRIMSTONE          = new Tile(TerrainType.Brimstone);

        Tile.CLAY               = new Tile(TerrainType.Clay);
        Tile.COAL               = new Tile(TerrainType.Coal);
        Tile.COPPER             = new Tile(TerrainType.Copper);
        Tile.DIAMOND            = new Tile(TerrainType.Diamond);
        Tile.DIRT               = new Tile(TerrainType.Dirt);
        Tile.EMERALD            = new Tile(TerrainType.Emerald);

        Tile.FELDSPAR           = new Tile(TerrainType.Feldspar);
        Tile.FIRESTONE          = new Tile(TerrainType.Firestone);
        Tile.FOSSIL             = new Tile(TerrainType.Fossil);
        Tile.GLOWSTONE          = new Tile(TerrainType.Glowstone);
        Tile.GOLD               = new Tile(TerrainType.Gold);
        Tile.GRANITE            = new Tile(TerrainType.Granite);

        Tile.ICE                = new Tile(TerrainType.Ice);
        Tile.IRON               = new Tile(TerrainType.Iron);
        Tile.JADE               = new Tile(TerrainType.Jade);
        Tile.LEAD               = new Tile(TerrainType.Lead);
        Tile.LIMESTONE          = new Tile(TerrainType.Limestone);

        Tile.MAGICITE           = new Tile(TerrainType.Magicite);
        Tile.MARBLE             = new Tile(TerrainType.Marble);
        Tile.MUD                = new Tile(TerrainType.Mud);
        Tile.MYTHRIL            = new Tile(TerrainType.Mythril);
        Tile.NETHERAK           = new Tile(TerrainType.Netherak);

        Tile.OBSIDIAN           = new Tile(TerrainType.Obsidian);
        Tile.QUARTZ             = new Tile(TerrainType.Quartz);
        Tile.RUBY               = new Tile(TerrainType.Ruby);

        Tile.SAND               = new Tile(TerrainType.Sand);
        Tile.SANDSTONE          = new Tile(TerrainType.Sandstone);
        Tile.SAPPHIRE           = new Tile(TerrainType.Sapphire);
        Tile.SCALE              = new Tile(TerrainType.Scale);
        Tile.SHALE              = new Tile(TerrainType.Shale);
        Tile.SILVER             = new Tile(TerrainType.Silver);
        Tile.SLATE              = new Tile(TerrainType.Slate);
        Tile.SLIME              = new Tile(TerrainType.Slime);
        Tile.SNOW               = new Tile(TerrainType.Snow);
        Tile.STEEL              = new Tile(TerrainType.Steel);

        Tile.TITANIUM           = new Tile(TerrainType.Titanium);
        Tile.TUNGSTEN           = new Tile(TerrainType.Tungsten);

        //Tile.VOID               = new Tile(TerrainType.Void);
    }


    // TILE RULES
}