﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Map
{
    [System.Serializable]
    public class Tile
    {
        public static Tile ADAMANTINE, ALUMINIUM, AMBER, AMETHYST, AMPHIBOLITE, AMYGDALITE;
        public static Tile BASALT, BISMUTH, BLOODSTONE, BRIMSTONE;
        public static Tile CLAY, COAL, COPPER, DIAMOND, DIRT, EMERALD;
        public static Tile FELDSPAR, FIRESTONE, FOSSIL, GLOWSTONE, GOLD, GRANITE;
        public static Tile ICE, IRON, JADE, LEAD, LIMESTONE;
        public static Tile MAGICITE, MARBLE, MUD, MYTHRIL, NETHERAK;
        public static Tile OBSIDIAN, QUARTZ, RUBY;
        public static Tile SAND, SANDSTONE, SAPPHIRE, SCALE, SHALE, SILVER, SLATE, SLIME, SNOW, STEEL;
        public static Tile TITANIUM, TUNGSTEN;
        public static Tile VOID;
        // Make terrain type called Gemerite, which drops random gems
        // Terrain type called Dragonite


        public TileData tileData;
        [Range(0, 100)]
        public float probability;


        // Constructor
        public Tile(TileData tileData, float probability = 100)
        {
            this.tileData = tileData;
            this.probability = probability;
        }

        public Tile(TerrainType type, float probability = 100)
        {
            SetTileType(type);
            this.probability = probability;
        }


        #region PROPERTIES (GET-SET) - MATERIAL, TYPE, INDEX, VALUE, COLOR, NAME
        // Material Type
        public MaterialType Material
        {
            get { return tileData.material; }
            set { tileData.material = value; }
        }


        // Terrain Type
        public TerrainType Type
        {
            get { return tileData.type; }
            set { tileData.type = value; }
        }


        // Tile Index
        public int Index
        {
            get { return tileData.index; }
            set { tileData.index = value; }
        }


        // Tile Value
        public float Value
        {
            get { return tileData.value; }
            set { tileData.value = value; }
        }


        // Tile Color
        public Color DebugColor
        {
            get { return tileData.debugColor; }
            set { tileData.debugColor = value; }
        }


        // Tile Name
        public string Name
        {
            get { return tileData.name; }
            set { tileData.name = value; }
        }
        #endregion


        // Set Tile Type
        public void SetTileType(TerrainType type)
        {
            switch (type)
            {
                default:
                case TerrainType.Adamantine: tileData = TileData.ADAMANTINE; break;
                case TerrainType.Aluminium: tileData = TileData.ALUMINIUM; break;
                case TerrainType.Amber: tileData = TileData.AMBER; break;
                case TerrainType.Amethyst: tileData = TileData.AMETHYST; break;
                case TerrainType.Amphibolite: tileData = TileData.AMPHIBOLITE; break;
                case TerrainType.Amygdalite: tileData = TileData.AMYGDALITE; break;

                case TerrainType.Basalt: tileData = TileData.BASALT; break;
                case TerrainType.Bismuth: tileData = TileData.BISMUTH; break;
                case TerrainType.Bloodstone: tileData = TileData.BLOODSTONE; break;
                case TerrainType.Brimstone: tileData = TileData.BRIMSTONE; break;

                case TerrainType.Clay: tileData = TileData.CLAY; break;
                case TerrainType.Coal: tileData = TileData.COAL; break;
                case TerrainType.Copper: tileData = TileData.COPPER; break;
                case TerrainType.Diamond: tileData = TileData.DIAMOND; break;
                case TerrainType.Dirt: tileData = TileData.DIRT; break;
                case TerrainType.Emerald: tileData = TileData.EMERALD; break;

                case TerrainType.Feldspar: tileData = TileData.FELDSPAR; break;
                case TerrainType.Firestone: tileData = TileData.FIRESTONE; break;
                case TerrainType.Fossil: tileData = TileData.FOSSIL; break;
                case TerrainType.Glowstone: tileData = TileData.GLOWSTONE; break;
                case TerrainType.Gold: tileData = TileData.GOLD; break;
                case TerrainType.Granite: tileData = TileData.GRANITE; break;

                case TerrainType.Ice: tileData = TileData.ICE; break;
                case TerrainType.Iron: tileData = TileData.IRON; break;
                case TerrainType.Jade: tileData = TileData.JADE; break;
                case TerrainType.Lead: tileData = TileData.LEAD; break;
                case TerrainType.Limestone: tileData = TileData.LIMESTONE; break;

                case TerrainType.Magicite: tileData = TileData.MAGICITE; break;
                case TerrainType.Marble: tileData = TileData.MARBLE; break;
                case TerrainType.Mud: tileData = TileData.MUD; break;
                case TerrainType.Mythril: tileData = TileData.MYTHRIL; break;
                case TerrainType.Netherak: tileData = TileData.NETHERAK; break;

                case TerrainType.Obsidian: tileData = TileData.OBSIDIAN; break;
                case TerrainType.Quartz: tileData = TileData.QUARTZ; break;
                case TerrainType.Ruby: tileData = TileData.RUBY; break;

                case TerrainType.Sand: tileData = TileData.SAND; break;
                case TerrainType.Sandstone: tileData = TileData.SANDSTONE; break;
                case TerrainType.Sapphire: tileData = TileData.SAPPHIRE; break;
                case TerrainType.Scale: tileData = TileData.SCALE; break;
                case TerrainType.Shale: tileData = TileData.SHALE; break;
                case TerrainType.Silver: tileData = TileData.SILVER; break;
                case TerrainType.Slate: tileData = TileData.SLATE; break;
                case TerrainType.Slime: tileData = TileData.SLIME; break;
                case TerrainType.Snow: tileData = TileData.SNOW; break;
                case TerrainType.Steel: tileData = TileData.STEEL; break;

                case TerrainType.Titanium: tileData = TileData.TITANIUM; break;
                case TerrainType.Tungsten: tileData = TileData.TUNGSTEN; break;

                case TerrainType.Void: tileData = TileData.VOID; break;
            }
        }
    }
}
