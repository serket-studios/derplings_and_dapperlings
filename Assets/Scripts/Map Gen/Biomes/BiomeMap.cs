﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeMap
{
    [Header("Map Size")]
    private int width;
    private int height;

    public int Width
    {
        get { return width; }
        set { width = value; }
    }

    public int Height
    {
        get { return height; }
        set { height = value; }
    }

    // Voronoi Map
    private float[,] voronoi;

    // Biome Map
    private Biome[,] biomeMap;
    public Biome[,] GetBiomeMap { get { return biomeMap; } }
    public Biome[,] SetBiomeMap { set { biomeMap = value; } }


    // Constructor
    public BiomeMap(float[,] voronoi, int width, int height)
    {
        this.voronoi = voronoi;

        this.biomeMap = BiomeNoise.Create(voronoi, width, height);
    }


    // Create
    public static Biome[,] Create(float[,] voronoi, int width, int height)
    {
        return new BiomeMap(voronoi, width, height).biomeMap;
    }
}
