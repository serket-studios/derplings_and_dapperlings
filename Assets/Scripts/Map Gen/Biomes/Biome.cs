﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Directions;
using Map;

[System.Serializable]
public class Biome
{
    public static Biome VOID, PLAINS, FOREST, SWAMP, DESERT, FROZEN, OIL, METAL, STONE, SEA, SKY, SURFACE, NETHER, CRYSTAL, MAGIC;
    public static Biome BORDER;

    public BiomeData biomeData;
    [Range(0, 100)] [Tooltip("Odds of this biome appearing in a row")]
    public float probability;

    // Border
    private bool isBiomeSeam;
    public bool IsBiomeSeam
    {
        get { return isBiomeSeam; }
        set { isBiomeSeam = value; }
    }


    // Constructor
    public Biome(BiomeData biomeData, float probability = 100)
    {
        this.biomeData = biomeData;
        this.probability = probability;
    }

    public Biome(BiomeType type, float probability = 100)
    {
        SetBiomeType(type);
        this.probability = probability;
    }


    #region PROPERTIES (GET-SET) - TYPE, INDEX, VALUE, COLOR, NAME, TILES
    // Biome Type
    public BiomeType Type
    {
        get { return biomeData.type; }
        set { biomeData.type = value; }
    }


    // Biome Index
    public int Index
    {
        get { return biomeData.index; }
        set { biomeData.index = value; }
    }


    // Biome Value
    public float Value
    {
        get { return biomeData.value; }
        set { biomeData.value = value; }
    }


    // Biome Color
    public Color DebugColor
    {
        get { return biomeData.color; }
        set { biomeData.color = value; }
    }


    // Biome Name
    public string Name
    {
        get { return biomeData.name; }
        set { biomeData.name = value; }
    }


    // Biome Tiles
    public List<Tile> TerrainTiles
    {
        get { return biomeData.GetTiles(); }
        set { biomeData.SetTiles(value); }
    }


    // Biome Tile Values
    public void SetTileValues()
    {
        this.biomeData.SetTileValues();
    }
    #endregion


    // Set Biome Type
    public void SetBiomeType(BiomeType type)
    {
        switch (type)
        {
            default:
            case BiomeType.Void: biomeData = BiomeData.VOID; break;
            case BiomeType.Plains: biomeData = BiomeData.PLAINS; break;
            case BiomeType.Forest: biomeData = BiomeData.FOREST; break;
            case BiomeType.Swamp: biomeData = BiomeData.SWAMP; break;
            case BiomeType.Desert: biomeData = BiomeData.DESERT; break;
            case BiomeType.Sea: biomeData = BiomeData.SEA; break;
            case BiomeType.Stone: biomeData = BiomeData.STONE; break;
            case BiomeType.Metal: biomeData = BiomeData.METAL; break;
            case BiomeType.Frozen: biomeData = BiomeData.FROZEN; break;
            case BiomeType.Magic: biomeData = BiomeData.MAGIC; break;
            case BiomeType.Crystal: biomeData = BiomeData.CRYSTAL; break;
            case BiomeType.Oil: biomeData = BiomeData.OIL; break;
            case BiomeType.Nether: biomeData = BiomeData.NETHER; break;
            case BiomeType.Surface: biomeData = BiomeData.SURFACE; break;
            case BiomeType.Sky: biomeData = BiomeData.SKY; break;

            // Border
            case BiomeType.Border: biomeData = BiomeData.BORDER; break;
        }
    }
}