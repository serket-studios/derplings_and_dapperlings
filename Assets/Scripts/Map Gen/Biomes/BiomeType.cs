﻿public enum BiomeType
{
    Void = 0,
    Plains = 1,
    Forest = 2,
    Swamp = 3,
    Desert = 4,
    Sea = 5,
    Stone = 6,
    Metal = 7,
    Frozen = 8,
    Magic = 9,
    Crystal = 10,
    Oil = 11,
    Nether = 12,
    Surface = 13,
    Sky = 14,

    Border = 100
}
