﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BiomeManager
{
    #region GET-SET BIOME AT COORDS
    // Set Biome
    public static void SetBiomeAtCoords(int x, int y, BiomeType type, Biome[,] biomeMap)
    {
        biomeMap[x, y] = new Biome(type);
    }

    // Get Biome
    public static Biome GetBiomeAtCoord(int x, int y, Biome[,] biomeMap)
    {
        return biomeMap[x, y];
    }

    public static Biome GetBiomeAtCoords(int x, int y)
    {
        return GetBiomeAtCoord(x, y, MapGenerator.i.GetBiomeMap());
    }
    #endregion


    // Set static biomes
    public static void SetStaticBiomes()
    {
        BiomeListSO biomeTypes = MapModes.i.ALL_BIOMES_LIST;


        BiomeData.VOID = biomeTypes.biomes[0].biomeData;
        BiomeData.PLAINS = biomeTypes.biomes[1].biomeData;
        BiomeData.FOREST = biomeTypes.biomes[2].biomeData;
        BiomeData.SWAMP = biomeTypes.biomes[3].biomeData;
        BiomeData.DESERT = biomeTypes.biomes[4].biomeData;
        BiomeData.SEA = biomeTypes.biomes[5].biomeData;
        BiomeData.STONE = biomeTypes.biomes[6].biomeData;
        BiomeData.METAL = biomeTypes.biomes[7].biomeData;
        BiomeData.FROZEN = biomeTypes.biomes[8].biomeData;
        BiomeData.MAGIC = biomeTypes.biomes[9].biomeData;
        BiomeData.CRYSTAL = biomeTypes.biomes[10].biomeData;
        BiomeData.OIL = biomeTypes.biomes[11].biomeData;
        BiomeData.NETHER = biomeTypes.biomes[12].biomeData;
        BiomeData.SURFACE = biomeTypes.biomes[13].biomeData;
        BiomeData.SKY = biomeTypes.biomes[14].biomeData;

        // Biomes
        Biome.VOID = new Biome(BiomeData.VOID);
        Biome.PLAINS = new Biome(BiomeData.PLAINS);
        Biome.FOREST = new Biome(BiomeData.FOREST);
        Biome.SWAMP = new Biome(BiomeData.SWAMP);
        Biome.DESERT = new Biome(BiomeData.DESERT);
        Biome.SEA = new Biome(BiomeData.SEA);
        Biome.STONE = new Biome(BiomeData.STONE);
        Biome.METAL = new Biome(BiomeData.METAL);
        Biome.FROZEN = new Biome(BiomeData.FROZEN);
        Biome.MAGIC = new Biome(BiomeData.MAGIC);
        Biome.CRYSTAL = new Biome(BiomeData.CRYSTAL);
        Biome.OIL = new Biome(BiomeData.OIL);
        Biome.NETHER = new Biome(BiomeData.NETHER);
        Biome.SURFACE = new Biome(BiomeData.SURFACE);
        Biome.SKY = new Biome(BiomeData.SKY);


        // Border
        int lastBiome = biomeTypes.biomes.Count - 1;
        BiomeData.BORDER = biomeTypes.biomes[lastBiome].biomeData;
        Biome.BORDER = new Biome(BiomeData.BORDER);
    }


    #region BIOME RULES
    // Apply current rules
    public static float ApplyCurrentRules(int x, int y, int width, int height, Vector3Int centroid, int index, float[] valuesArray)
    {
        BiomeRuleListSO rules = MapModes.i.currentBiomeRulesList;
        index = rules.ApplyAllRules(width, height, centroid, index);

        float value = 0;

        // Border
        if (index == Biome.BORDER.Index)
            value = Biome.BORDER.Value;

        // Cull Sky
        else if (y > MapGenerator.SkyHeight || y > MapGenerator.SheerHeight)
            value = 0;

        // Biomes
        else if (index < valuesArray.Length)
            value = valuesArray[index];

        return value;
    }


    // Apply Seams Rules
    public static Color ApplyCurrentSeamRules(int x, int y, Biome biome, Color defaultColor)
    {
        BiomeSeamRuleListSO rules = MapModes.i.currentBiomeSeamRulesList;

        defaultColor = rules.ApplyAllRules(x, y, biome.Type, defaultColor);

        return defaultColor;
    }
    #endregion
}
