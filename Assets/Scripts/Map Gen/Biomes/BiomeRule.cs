﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeRule
{
    public static BiomeRule SKY, SURFACE, STONE_METAL, CRYSTAL_MAGIC, OIL_NETHER;
    public static BiomeRule BORDER_LEFT, BORDER_RIGHT, BORDER_TOP, BORDER_BOTTOM, BORDER;

    public List<Biome> biomesList;

    [Range(0, 1)]
    public float xMin, xMax, yMin, yMax;


    // Constructor
    public BiomeRule(List<Biome> biomesList, Vector2 xSpan, Vector2 ySpan)
    {
        this.biomesList = biomesList;

        // Span X, Y
        this.xMin = Mathf.Clamp(xSpan.x, 0, 100) * 0.01f;
        this.xMax = Mathf.Clamp(xSpan.y, xMin, 100) * 0.01f;
        this.yMin = Mathf.Clamp(ySpan.x, 0, 100) * 0.01f;
        this.yMax = Mathf.Clamp(ySpan.y, yMin, 100) * 0.01f;

        if (xMin > xMax) xMin = xMax;
        if (yMin > yMax) yMin = yMax;
    }



    // Apply Rule
    public static int Apply(BiomeRule rule, int width, int height, Vector3Int centroid, int defaultIndex = 0)
    {
        int xMin = Mathf.RoundToInt(rule.xMin * width);
        int xMax = Mathf.RoundToInt(rule.xMax * width);
        int yMin = Mathf.RoundToInt(rule.yMin * height);
        int yMax = Mathf.RoundToInt(rule.yMax * height);


        // Check if centroid is in bounds
        if (centroid.x >= xMin && centroid.x <= xMax && centroid.y >= yMin && centroid.y <= yMax)
        {
            defaultIndex = GetRandomBiome(rule);
        }

        return defaultIndex;
    }


    // Get Random Biome
    private static int GetRandomBiome(BiomeRule rule)
    {
        float totalWeight = 0;
        int index = 0;

        // Calculate total weight
        foreach (Biome biome in rule.biomesList)
        {
            totalWeight += biome.probability;
        }


        // Seed
        System.Random prng = MapGenerator.PRNG;

        float randValue = prng.Next(0, 10000) * 0.0001f * totalWeight;
        float value = rule.biomesList[0].probability;

        // Search through list
        while (value < randValue && index < rule.biomesList.Count - 1)
        {
            index++;
            value += rule.biomesList[index].probability;
        }

        return rule.biomesList[index].Index;
    }
}
