﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeSeamRule
{
    public static BiomeSeamRule NONE, SURFACE, GREENERY, STONE_METAL, THERMAL, NETHER;

    [Range(0, 1)]
    public float xMin, xMax, yMin, yMax;

    List<BiomeType> typesList;

    List<BiomeSeam> seamsList;


    // Constructor
    public BiomeSeamRule(List<BiomeSeam> seamsList, Vector2 xSpan, Vector2 ySpan)
    {
        this.seamsList = seamsList;

        // Span X, Y
        this.xMin = Mathf.Clamp(xSpan.x, 0, 100) * 0.01f;
        this.xMax = Mathf.Clamp(xSpan.y, xMin, 100) * 0.01f;
        this.yMin = Mathf.Clamp(ySpan.x, 0, 100) * 0.01f;
        this.yMax = Mathf.Clamp(ySpan.y, yMin, 100) * 0.01f;

        if (xMin > xMax) xMin = xMax;
        if (yMin > yMax) yMin = yMax;
    }


    public BiomeSeamRule(List<BiomeSeam> seamsList, List<BiomeType> types)
    {
        this.seamsList = seamsList;
        this.typesList = types;
    }


    // Apply to Depth
    public static Color Apply(BiomeSeamRule rule, int x, int y, Color defaultColor)
    {
        int xMin = Mathf.RoundToInt(rule.xMin * MapGenerator.Width);
        int xMax = Mathf.RoundToInt(rule.xMax * MapGenerator.Width);
        int yMin = Mathf.RoundToInt(rule.yMin * MapGenerator.Height);
        int yMax = Mathf.RoundToInt(rule.yMax * MapGenerator.Height);


        // Check if tile is in bounds
        if (x >= rule.xMin && x <= xMax && y >= yMin && y <= yMax)
        {
            defaultColor = GetRandomColor(rule);
        }

        return defaultColor;
    }


    // Apply to Type
    public static Color Apply(BiomeSeamRule rule, BiomeType type, Color defaultColor)
    {
        if (rule.typesList.Contains(type))
        {
            defaultColor = GetRandomColor(rule);
        }

        return defaultColor;
    }


    // Get Random Color
    private static Color GetRandomColor(BiomeSeamRule rule)
    {
        float totalWeight = 0;
        int index = 0;

        // Calculate total weight
        foreach (BiomeSeam seam in rule.seamsList)
        {
            totalWeight += seam.probability;
        }


        // Seed
        System.Random prng = MapGenerator.PRNG;

        float randValue = prng.Next(0, 10000) * 0.0001f * totalWeight;
        float value = rule.seamsList[0].probability;

        // Search through list
        while (value < randValue && index < rule.seamsList.Count - 1)
        {
            index++;
            value += rule.seamsList[index].probability;
        }

        return rule.seamsList[index].debugColor;
    }
}
