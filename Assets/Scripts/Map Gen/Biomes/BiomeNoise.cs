﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BiomeNoise
{
    // Create
    public static Biome[,] Create(float[,] voronoi, int width, int height)
    {
        Biome[,] biomeMap = new Biome[width, height];

        // Biomes by reverse index
        List<Biome> biomes = MapModes.i.currentBiomeList.GetBiomes();
        BiomeListSO.SetBiomeValues(biomes);
        biomes.Reverse();


        // Loop through map
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                // Get voronoi value
                float v = voronoi[x, y];

                // Cycle through biomes
                for (int i = 0; i < biomes.Count; i++)
                {
                    // Return closest match
                    if (v <= biomes[i].Value)
                    {
                        BiomeManager.SetBiomeAtCoords(x, y, biomes[i].Type, biomeMap);

                        // Mark seams
                        Biome biome = biomeMap[x, y];
                        biome.IsBiomeSeam = false;

                        MarkBiomeSeams(x, y, biomeMap, v, voronoi);
                    }
                }
            }
        }

        return biomeMap;
    }


    // Mark Biome Seams
    public static void MarkBiomeSeams(int x, int y, Biome[,] biomeMap, float v, float[,] voronoi)
    {
        if (NoiseUtils.InBounds(x, y, MapGenerator.SeamsBorder) && y < MapGenerator.SkyHeight)
        {
            Biome biome = biomeMap[x, y];

            // Neighboring types are different
            if (v != voronoi[x + 1, y] ||
                v != voronoi[x - 1, y] ||
                v != voronoi[x, y + 1] ||
                v != voronoi[x, y - 1])
            {
                biome.IsBiomeSeam = true;
            }
        }
    }
}
