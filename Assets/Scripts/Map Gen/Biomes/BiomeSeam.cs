﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BiomeSeam
{
    public Color debugColor;
    public float probability;

    // Constructor
    public BiomeSeam(Color color, float probability)
    {
        this.debugColor = color;
        this.probability = probability;
    }
}
