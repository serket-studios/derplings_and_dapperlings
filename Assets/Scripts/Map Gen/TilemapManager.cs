﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Directions;

public class TilemapManager : MonoBehaviour
{
    // Singleton
    public static TilemapManager i { get; private set; }

    public Tilemap backgroundMap;
    public Tilemap foregroundMap;
    public Tile testTile;

    public Color seamColor1;
    public Color seamColor2;
    public Color seamColor3;
    public Color seamColor4;

    Vector3Int pos;


    // Awake
    private void Awake()
    {
        i = this;
    }


    // Update Map
    public void UpdateMap()
    {
        backgroundMap.ClearAllTiles();
        foregroundMap.ClearAllTiles();

        Biome[,] biomeMap = MapGenerator.i.GetBiomeMap();
        Map.Tile[,] tileMap = MapGenerator.i.GetTileMap();
        
        Biome biome;
        Map.Tile tile;

        // Loop through cells
        for (int x = 0; x < MapGenerator.Width; x++)
        {
            for (int y = 0; y < MapGenerator.Height; y++)
            {
                biome = biomeMap[x, y];
                tile = tileMap[x, y];

                // Background
                pos = new Vector3Int(x, y, 0);
                testTile.color = biome.DebugColor;
                backgroundMap.SetTile(pos, testTile);


                
                /// CONSIDER MOVING TILE NOISE TO WITHIN THE FOR-LOOPS OF BIOME NOISE
                /// MAKE MORE DEPTH LAYERS FOR BIOMES - ABOUT 20 TOTAL, SO EVERY 5%
                /// MAKE A VERSION WITH CONCENTRIC RINGS


                // Seams
                if (biome.IsBiomeSeam)
                    HandleSeams(x, y, biome);

                // Handle Border
                if (biome.Type == BiomeType.Border)
                    HandleBorder(x, y);

                // Sky
                if (biome.Type == BiomeType.Void)
                    HandleSky(x, y);

                // Foreground
                else
                    HandleTerrainTiles(x, y, tile);
            }
        }
    }


    // Handle Terrain Tiles
    private void HandleTerrainTiles(int x, int y, Map.Tile tile)
    {
        Tile terrainTile = testTile;
        terrainTile.color = (tile != null) ? tile.DebugColor : Color.clear;
        foregroundMap.SetTile(pos, terrainTile);
    }


    // Handle Seams
    private void HandleSeams(int x, int y, Biome biome)
    {
        testTile.color = Color.clear;
        testTile.color = BiomeManager.ApplyCurrentSeamRules(x, y, biome, testTile.color);

        // Set Tile
        Vector3Int pos = new Vector3Int(x, y, 0);
        foregroundMap.SetTile(pos, testTile);
    }


    private void HandleSeamsOld(int x, int y, Biome biome)
    {
        System.Random prng = MapGenerator.PRNG;
        float randomValue = prng.Next(0, 10000) * 0.0001f;


        // Surface
        if (y < MapGenerator.SkyHeight * 1.5f && y > MapGenerator.SeamsHeight)
            testTile.color = seamColor2;

        // Middle
        if (y < MapGenerator.SeamsHeight)
        {
            // Frozen or desert
            if (biome.Type == BiomeType.Frozen || biome.Type == BiomeType.Desert)
            {
                if (randomValue < MapGenerator.SeamFill)
                    testTile.color = seamColor4;
            }

            else
                testTile.color = (randomValue < MapGenerator.SeamFill) ? seamColor1 : seamColor2;
        }

        // Bottom
        if (y < MapGenerator.SeamsHeightRaw)
            testTile.color = (randomValue < MapGenerator.SeamFill) ? seamColor3 : seamColor1;


        // Set Tile
        Vector3Int pos = new Vector3Int(x, y, 0);
        foregroundMap.SetTile(pos, testTile);
    }


    // Handle Sky
    private void HandleSky(int x, int y)
    {
        backgroundMap.SetTile(pos, null);
        foregroundMap.SetTile(pos, null);
    }

    // Handle Border
    private void HandleBorder(int x, int y)
    {
        //Vector3Int pos = new Vector3Int(x, y, 0);
        //testTile.color = Biome.BORDER.DebugColor;
        //backgroundMap.SetTile(pos, testTile);
        foregroundMap.SetTile(pos, testTile);
    }
}
