﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CreateAssetMenu(fileName = "Tile Data", menuName = "Data/Tile Data", order = 10)]
public class TileData : ScriptableObject
{
    // Statics
    public static TileData ADAMANTINE, ALUMINIUM, AMBER, AMETHYST, AMPHIBOLITE, AMYGDALITE;
    public static TileData BASALT, BISMUTH, BLOODSTONE, BRIMSTONE;
    public static TileData CLAY, COAL, COPPER, DIAMOND, DIRT, EMERALD;
    public static TileData FELDSPAR, FIRESTONE, FOSSIL, GLOWSTONE, GOLD, GRANITE;
    public static TileData ICE, IRON, JADE, LEAD, LIMESTONE;
    public static TileData MAGICITE, MARBLE, MUD, MYTHRIL, NETHERAK;
    public static TileData OBSIDIAN, QUARTZ, RUBY;
    public static TileData SAND, SANDSTONE, SAPPHIRE, SCALE, SHALE, SILVER, SLATE, SLIME, SNOW, STEEL;
    public static TileData TITANIUM, TUNGSTEN;
    public static TileData VOID;
    // Make terrain type called Gemerite, which drops random gems
    // Terrain type called Dragonite


    public TerrainType type;
    public MaterialType material;
    public int index;
    public float value;

    public Color debugColor;

    // Sprite Data
    // Logic Data
    // Temperature
    // Walkable, Destructable
    // Solid, liquid, gas


    // Constructor
    public TileData(TerrainType type, int index, Color debugColor)
    {
        this.index = index;
        this.value = 0;

        this.debugColor = debugColor;

    }


    // Initialize
    public void Init(bool renameAsset = false)
    {
        this.name = type.ToString();
        this.index = (int)type;
        this.value = index * 0.01f;

        // Rename Asset
        if (renameAsset) 
        {
            string assetPath = AssetDatabase.GetAssetPath(this.GetInstanceID());
            AssetDatabase.RenameAsset(assetPath, this.name);
            AssetDatabase.SaveAssets(); 
        }
    }
}
