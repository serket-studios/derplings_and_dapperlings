﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Map;

[System.Serializable]
[CreateAssetMenu(fileName = "Tile List SO", menuName = "Data/Tile List SO", order = 11)]
public class TileListSO : ScriptableObject
{
    public List<TileData> tileDataList;
    public List<Tile> tiles;
    private List<float> valueList;

    public bool renameAssets;

    // Constructors
    public TileListSO(TileListSO list)
    {
        this.tiles = list.tiles;
        this.valueList = list.valueList;
    }

    public TileListSO(List<Tile> tiles)
    {
        this.tiles = tiles;
        this.valueList = GetTileValues();
    }


    // Initialize
    public void Init()
    {
        List<Tile> t = new List<Tile>();

        // Make tiles from data
        foreach(TileData tileData in tileDataList)
        {
            //TileData tileData = tileDataList[i];
            tileData.Init(renameAssets);

            t.Add(new Tile(tileData));
        }

        this.tiles = t;
    }


    #region GET-SET - TILES, VALUES
    // Get Tiles
    public List<Tile> GetTiles(bool sorted = true, bool reverse = false)
    {
        return GetTiles(tiles, sorted, reverse);
    }


    public List<Tile> GetTiles(List<Tile> tiles, bool sorted = true, bool reverse = false)
    {
        if (sorted) return SortByIndex(tiles, reverse);

        return tiles;
    }



    // Set Tile Values
    public static void SetTileValues(List<Tile> tiles)
    {
        tiles = SortByIndex(tiles);

        for (int i = 0; i < tiles.Count; i++)
        {
            tiles[i].tileData.Init();
        }
    }


    public List<float> GetTileValues(bool sorted = true, bool reverse = false)
    {
        valueList = new List<float>();

        if (sorted)
            tiles = SortByValue(tiles, reverse);

        for (int i = 0; i < tiles.Count; i++)
        {
            valueList.Add(tiles[i].Value);
        }

        return valueList;
    }
    #endregion


    #region SORTING - NAME, INDEX, VALUE
    // Sort by Name
    public static List<Tile> SortByName(List<Tile> tiles, bool reverse = false)
    {
        // Z-A
        if (reverse) tiles.Sort((a, b) => { return b.Name.CompareTo(a.Name); });

        // A-Z
        else tiles.Sort((a, b) => { return a.Name.CompareTo(b.Name); });

        return tiles;
    }


    // Sort by Index
    public static List<Tile> SortByIndex(List<Tile> tiles, bool reverse = false)
    {
        // Inf-0
        if (reverse) tiles.Sort((a, b) => { return b.Index.CompareTo(a.Index); });

        // 0-Inf
        else tiles.Sort((a, b) => { return a.Index.CompareTo(b.Index); });

        return tiles;
    }


    // Sort by Value
    public static List<Tile> SortByValue(List<Tile> tiles, bool reverse = false)
    {
        // Inf-0
        if (reverse) tiles.Sort((a, b) => { return b.Value.CompareTo(a.Value); });

        // 0-Inf
        else tiles.Sort((a, b) => { return a.Value.CompareTo(b.Value); });

        return tiles;
    }
    #endregion
}
