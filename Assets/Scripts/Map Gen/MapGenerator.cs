﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Map;

public class MapGenerator : MonoBehaviour
{
    // Singleton
    public static MapGenerator i { get; private set; }
    public static int Width { get { return MapGenerator.i.mapWidth; } }
    public static int Height { get { return MapGenerator.i.mapHeight; } }

    public static System.Random PRNG;


    [Header("Map Size")]
    #region MAP SIZE, BORDER
    public MapModes.MapMode mapMode;
    [Range(1, 500)]
    public int mapWidth = 250;
    [Range(1, 500)]
    public int mapHeight = 250;
    [Range(0, 100)]
    [SerializeField] private float outerBorderSize = 5;
    [Range(0, 100)]
    [SerializeField] private float innerBorderSize = 10;

    public static Vector2Int BorderSize
    {
        get { return new Vector2Int(Mathf.RoundToInt(MapGenerator.i.outerBorderSize * 100f / Width), 
            Mathf.RoundToInt(MapGenerator.i.outerBorderSize * 100f/ Height)); }
    }

    public static Vector2Int InnerBorderSize
    {
        get { return new Vector2Int(Mathf.RoundToInt(MapGenerator.i.innerBorderSize * 100f / Width), 
            Mathf.RoundToInt(MapGenerator.i.innerBorderSize * 100f / Height)); }
    }

    public static float OuterBorderRaw { get { return MapGenerator.i.outerBorderSize; } }
    public static float InnerBorderRaw { get { return MapGenerator.i.innerBorderSize; } }
    #endregion

    [Header("Seed")]
    public int seed;
    public bool useRandomSeed;
    
    [Header("Gaps")]
    #region GAPS - SHEER, SKY HEIGHT
    [Range(0, 100)]
    [SerializeField] private int sheerHeight = 15;            // Plateau land
    [Range(0, 100)]
    [SerializeField] private int skyHeight = 25;              // Sky region
    [Range(0, 1)]
    public float voidThreshold = 0.25f;    // Create voids
    public static float SheerHeight
    {
        get { return Mathf.RoundToInt(Height - Height * (MapGenerator.i.sheerHeight * .01f)); }
    }
    public static float SheerHeightRaw
    {
        get { return Mathf.RoundToInt(Height * MapGenerator.i.sheerHeight * 0.01f); }
    }
    public static float SheerHeightRaw_Percent
    {
        get { return MapGenerator.i.sheerHeight; }
    }

    public static float SkyHeight
    {
        get { return Mathf.RoundToInt(Height - Height * (MapGenerator.i.skyHeight * .01f)); }
    }
    public static float SkyHeightRaw
    {
        get { return Mathf.RoundToInt(Height * MapGenerator.i.skyHeight * 0.01f); }
    }
    public static float SkyHeightRaw_Percent
    {
        get { return MapGenerator.i.skyHeight; }
    }
    #endregion

    [Header("Seams")]
    #region SEAMS - HEIGHT, BORDER, FILL
    [Range(0, 100)]
    [SerializeField] private float seamsHeight = 35;            // Seams form below this depth
    [Range(1, 100)]
    [SerializeField] private float seamsBorder = 15;            // How close seams get to edge}
    [Range(0, 100)]
    [SerializeField] private float seamsFillPercent = 80f;   // How much of the seam is filled (leaving gaps) 

    public static float SeamFill
    {
        get { return MapGenerator.i.seamsFillPercent * 0.01f; }
    }
    public static float SeamFillRaw
    {
        get { return MapGenerator.i.seamsFillPercent; }
    }
    public static float SeamsHeight { 
        get { return Mathf.RoundToInt(Height - Height * (MapGenerator.i.seamsHeight * .01f)); }
    }
    public static float SeamsHeightRaw { 
        get { return Mathf.RoundToInt(Height * MapGenerator.i.seamsHeight * 0.01f); }  
    }
    public static float SeamsHeightRaw_Percent
    {
        get { return MapGenerator.i.seamsHeight; }
    }

    public static Vector2Int SeamsBorder {
        get { return new Vector2Int(Mathf.RoundToInt(Width * (MapGenerator.i.seamsBorder * 0.01f)), 
            Mathf.RoundToInt(Height * (MapGenerator.i.seamsBorder * 0.01f))); }
    }
    public static Vector2 SeamsBorderRaw_Percent
    {
        get { return new Vector2(MapGenerator.i.seamsBorder, MapGenerator.i.seamsBorder); }
    }
    #endregion

    [Header("Perlin Settings")]
    #region PERLIN SETTINGS
    [Range(0, 100)] [Tooltip("Scale: Number that determines at what distance to view the noisemap.  " +
        "You can use it to zoom out (bigger scale) or in (smaller scale).")]
    public float scale = 10;
    [Range(0, 8)] [Tooltip("Octaves: The number of levels of detail you want your perlin noise to have.  " +
        "Each octave adds a layer of detail to the surface. \n\n" +
        "For example: octave 1 could be mountains, octave 2 could be boulders, octave 3 could be the rocks.")]
    public int octaves = 3;
    [Range(0, 5)] [Tooltip("Lacunarity: Number that determines how much detail is added or removed at each octave (adjusts frequency). \n\n" +
        "Lacunarity of more than 1 means that each octave will increase its level of fine grained detail (increased frequency). " +
        "Lacunarity of 1 means that each octave will have the same level of detail. " +
        "Lacunarity of less than one means that each octave will get smoother. \n\n" +
        "The last two are usually undesirable so a lacunarity of 2 works quite well.")]
    public float lacunarity = 2;
    [Range(0, 5)] [Tooltip("Persistence: Number that determines how much each octave contributes to the overall shape (adjusts amplitude).  \n\n" +
        "Persistence determines how much each octave contributes to the overall structure of the noise map. If your persistence is 1 all " +
        "octaves contribute equally. If your persistence is more than 1, successive octaves contribute more and you get something closer " +
        "to regular noise. \n\n" +
        "A more default setting would be a persistence of less than 1.0 which will decrease the effect of later octaves.")]
    public float persistence = 1f;
    [Tooltip("Offset: Select a different origin point for the noise map.")]
    public Vector2 offset;
    #endregion

    [Header("Voronoi Settings")]
    #region VORONOI SETTINGS
    [Range(10, 25)]
    public int biomeSize = 10;
    private int numBiomes;
    public int NumBiomes
    {
        get { return numBiomes; }
        set { numBiomes = value; }
    }
    [Range(0, 100)]
    public int shapeDistortion = 50;
    public Vector2Int distortionLimits = new Vector2Int(10, 100);
    public bool useRandomDistortion;
    #endregion


    [Header("")]
    private Biome[,] biomeMap;
    private Tile[,] tileMap;

    public Biome[,] GetBiomeMap() { return biomeMap; }
    public Tile[,] GetTileMap() { return tileMap; }


    /// <summary>
    /// --------------------------------------------------- MAP GENERATION ----------------------------------------------------
    ///                                                 (Awake, Generate Map)
    /// -----------------------------------------------------------------------------------------------------------------------
    /// </summary>
    #region MAP GENERATION
    // Awake
    private void Awake()
    {
        i = this;
    }


    // Start is called on the first frame
    private void Start()
    {
        // Testing
        GenerateMap();
        CodeMonkey.Utils.FunctionPeriodic.Create(GenerateMap, 0.1f);
    }


    // Generate Map
    private void GenerateMap()
    {
        // Set current list
        MapModes.i.SetMapModeRules(mapMode);

        // Calculate number of biomes
        NumBiomes = Mathf.RoundToInt((Width * Height / (biomeSize * biomeSize)));
        NumBiomes = Mathf.Clamp(NumBiomes, 1, Width * Height);

        Camera.main.transform.position = new Vector3(mapWidth * 0.5f, mapHeight * 0.5f, -10f);
        Camera.main.orthographicSize = mapHeight * 0.5f;

        // Create biome map
        biomeMap = BiomeMap.Create(CreateVoronoiNoise(), mapWidth, mapHeight);

        // Create tile map
        tileMap = TileNoise.Create(CreatePerlinNoise(), mapWidth, mapHeight);


        // Update Map
        TilemapManager.i.UpdateMap();

        // Initialize Statics
        // Set Map Parameters
        // Generate Voronoi Noise
        // Assign Biomes
        // Make Border
        // Generate Perlin Noise
        // Run Internal Logic on Perlin Noise
        // Create Gaps
        // .....................
        // Set Liquids
        // Set Gases
        // Set Plants
        // Set Animals
        // Set Structures
        // Set Starting Portal
    }
    #endregion


    /// <summary>
    /// --------------------------------------------------- GET-SET VALUES ----------------------------------------------------
    ///                                        (Biome Values, Seed, Voronoi Distortion)
    /// -----------------------------------------------------------------------------------------------------------------------
    /// </summary>
    #region GET VALUES, CREATE SEED
    // Get Biome Values
    public float[] GetBiomeValues()
    { 
        BiomeListSO.SetBiomeValues(MapModes.i.currentBiomeList.GetBiomes());
        return MapModes.i.currentBiomeList.GetBiomeValues().ToArray();
    }


    // Create Seed
    public int CreateSeed()
    {
        int newSeed = useRandomSeed ? Random.Range(0, int.MaxValue) : seed;
        newSeed = Mathf.Clamp(newSeed, 0, int.MaxValue);

        PRNG = new System.Random(newSeed);

        return seed = newSeed;
    }


    // Get Seed
    public int GetSeed()
    {
        return seed;
    }


    // Create Voronoi Distortion
    public int CreateVoronoiDistortion()
    {
        System.Random prng = PRNG;
        int randomValue = prng.Next(distortionLimits.x, distortionLimits.y);

        int temp = useRandomDistortion ? randomValue : shapeDistortion;
        temp = Mathf.Clamp(temp, distortionLimits.x, distortionLimits.y);

        return shapeDistortion = temp;
    }
    #endregion


    /// <summary>
    /// ---------------------------------------------------- NOISE GENERATION ----------------------------------------------------
    ///                                                  (Perlin, Voronoi, Biome)
    /// --------------------------------------------------------------------------------------------------------------------------
    /// </summary>
    /// 
    #region
    
    /// <summary> Perlin Noise
    /// </summary>
    public static float[,] CreatePerlinNoise() 
    {
        return PerlinNoise.Create(MapGenerator.i.mapWidth, MapGenerator.i.mapHeight, MapGenerator.i.CreateSeed(),
                    MapGenerator.i.scale, MapGenerator.i.octaves, MapGenerator.i.lacunarity, 
                    MapGenerator.i.persistence, MapGenerator.i.offset);
    }


    // Voronoi Noise
    public static float[,] CreateVoronoiNoise()
    {
        return VoronoiNoise.Create(MapGenerator.i.mapWidth, MapGenerator.i.mapHeight, 
            MapGenerator.i.NumBiomes, MapGenerator.i.GetBiomeValues(), MapGenerator.i.skyHeight, MapGenerator.i.CreateSeed(), MapGenerator.i.CreateVoronoiDistortion());
    }


    // Biome Noise
    public static Biome[,] CreateBiomeNoise()
    {
        float[,] voronoi = CreateVoronoiNoise();

        return BiomeNoise.Create(voronoi, MapGenerator.i.mapWidth, MapGenerator.i.mapHeight);
    }
    #endregion
}
