﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapModes : MonoBehaviour
{
    // Singleton
    public static MapModes i { get; private set; }

    // Make mode
    public enum MapMode
    {
        Default, Hot, Cold
    }

    // Current rules
    public BiomeListSO currentBiomeList { get; set; }
    public BiomeRuleListSO currentBiomeRulesList { get; private set; }
    public BiomeSeamRuleListSO currentBiomeSeamRulesList { get; private set; }

    // Current seams rules
    // Current sky rules
    // Current sheer rules

    [Header("Biomes")]
    public BiomeListSO ALL_BIOMES_LIST;
    public TileListSO ALL_TERRAIN_TILES_LIST;

    [Header("Biome Rules")]
    public BiomeRuleListSO defaultRules;
    public BiomeRuleListSO hotRules;
    public BiomeRuleListSO coldRules;

    [Header("Biome Seam Rules")]
    public BiomeSeamRuleListSO defaultSeamRules;


    // Awake
    private void Awake()
    {
        i = this;
        Init();
    }


    // Initialization
    public void Init()
    {
        // Reset
        ALL_TERRAIN_TILES_LIST.Init();
        ALL_TERRAIN_TILES_LIST.GetTiles();
        ALL_BIOMES_LIST.GetBiomes();

        // Setup
        TileManager.SetStaticTiles();
        BiomeManager.SetStaticBiomes();
    }


    // Set Map Mode
    public void SetMapModeRules(MapMode mapMode)
    {
        SetBiomeRules(mapMode);
        SetSeamRules(mapMode);
    }


    // Set Mode
    public void SetBiomeRules(MapMode mapMode)
    {
        currentBiomeList = ALL_BIOMES_LIST;

        switch (mapMode)
        {
            default:
            case MapMode.Default:
                currentBiomeRulesList = defaultRules;
                break;
            case MapMode.Hot:
                currentBiomeRulesList = hotRules;
                break;
            case MapMode.Cold:
                currentBiomeRulesList = coldRules;
                break;
        }
    }


    // Set Seams rules
    public void SetSeamRules(MapMode mapMode)
    {
        switch (mapMode) 
        {
            default:
            case MapMode.Default:
                currentBiomeSeamRulesList = defaultSeamRules;
                break;
            case MapMode.Hot:
                currentBiomeSeamRulesList = defaultSeamRules;
                break;
            case MapMode.Cold:
                currentBiomeSeamRulesList = defaultSeamRules;
                break;
        }
    }
}