﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Biome Seam Rule List SO", menuName = "Data/Biome Seam Rule List SO", order = 5)]
public class BiomeSeamRuleListSO : ScriptableObject
{
    public List<BiomeSeamRuleSO> rulesList;


    // Apply all rules
    public Color ApplyAllRules(int x, int y, BiomeType biomeType, Color defaultColor)
    {
        for (int i = 0; i < rulesList.Count; i++)
        {
            defaultColor = rulesList[i].Apply(x, y, biomeType, defaultColor);
        }

        return defaultColor;
    }
}
