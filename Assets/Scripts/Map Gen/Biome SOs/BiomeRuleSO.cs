﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Biome Rule SO", menuName = "Data/Biome Rule SO", order = 2)]
public class BiomeRuleSO : ScriptableObject
{
    [Header("Boundary")]
    public Vector2 xSpan;
    public Vector2 ySpan;

    public bool SkyAsYMin, SkyAsYMax;
    public bool SheerAsYMin, SheerAsYMax;
    public bool PadBorderX, PadBorderY;

    [Space(10)]
    public List<Biome> biomesList;

    // Apply
    public int Apply(int width, int height, Vector3Int centroid, int defaultIndex = 0)
    {
        if (SkyAsYMin) ySpan.x = 100 - MapGenerator.SkyHeightRaw_Percent;
        if (SkyAsYMax) ySpan.y = 100 - MapGenerator.SkyHeightRaw_Percent;

        if (SheerAsYMin) ySpan.x = 100 - MapGenerator.SheerHeightRaw_Percent;
        if (SheerAsYMax) ySpan.y = 100 - MapGenerator.SheerHeightRaw_Percent;

        if (PadBorderX)
        {
            xSpan.x = MapGenerator.InnerBorderRaw;
            xSpan.y = 100 - MapGenerator.InnerBorderRaw;
        }

        if (PadBorderY) ySpan.x = MapGenerator.InnerBorderRaw;

        BiomeRule rule = new BiomeRule(biomesList, xSpan, ySpan);
        return BiomeRule.Apply(rule, width, height, centroid, defaultIndex);
    }
}
