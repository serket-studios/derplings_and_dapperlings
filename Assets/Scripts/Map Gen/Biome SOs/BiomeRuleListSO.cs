﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Biome Rule List SO", menuName = "Data/Biome Rule List SO", order = 3)]
public class BiomeRuleListSO : ScriptableObject
{
    public List<BiomeRuleSO> rulesList;


    // Apply all rules
    public int ApplyAllRules(int width, int height, Vector3Int centroid, int defaultIndex = 0)
    {
        for (int i = 0; i < rulesList.Count; i++)
        {
            defaultIndex = rulesList[i].Apply(width, height, centroid, defaultIndex);
        }

        return defaultIndex;
    }
}
