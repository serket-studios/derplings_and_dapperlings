﻿/* ------------------ Biome Data List SO ---------------------
 
    Scriptable Object List to hold Biome Data SO's for easy
    reference.  Populate with Biome Data SO's.
 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Biome List SO", menuName = "Data/Biome List SO", order = 1)]
public class BiomeListSO : ScriptableObject
{
    public List<Biome> biomes;
    private List<float> valueList;

    // Constructor
    public BiomeListSO(BiomeListSO list)
    {
        this.biomes = list.biomes;
        this.valueList = list.valueList;
    }

    public BiomeListSO(List<Biome> biomes)
    {
        this.biomes = biomes;
        this.valueList = GetBiomeValues();
    }


    // Get Biomes
    public List<Biome> GetBiomes(bool sorted = true, bool reverse = false)
    {
        return GetBiomes(biomes, sorted, reverse);
    }


    public List<Biome> GetBiomes(List<Biome> biomes, bool sorted = true, bool reverse = false)
    {
        if (sorted) return SortByIndex(biomes, reverse);

        return biomes;
    }


    // Set Values
    public static void SetBiomeValues(List<Biome> biomes)
    {
        biomes = SortByIndex(biomes);

        for (int i = 0; i < biomes.Count; i++)
        {
            //biomes[i].Value = biomes[i].Index * 1f / biomes.Count;
            biomes[i].biomeData.Init();
        }
    }


    // Get Values
    public List<float> GetBiomeValues(bool sorted = true, bool reverse = false)
    {
        valueList = new List<float>();

        if (sorted)
            biomes = SortByValue(biomes, reverse);

        for (int i = 0; i < biomes.Count; i++)
        {
            valueList.Add(biomes[i].Value);
        }

        return valueList;
    }


    /// ----------------------------------------------------------------------------------------------------------------------------------------
    ///                                                           SORT BY
    ///                                                      (Name, Index, Value)
    /// ----------------------------------------------------------------------------------------------------------------------------------------
    #region
    // Sort by Type
    public static List<Biome> SortByType(List<Biome> biomes, bool reverse = false)
    {
        // Z-A
        if (reverse) biomes.Sort((a, b) => { return b.Type.CompareTo(a.Type); });

        // A-Z
        else biomes.Sort((a, b) => { return a.Type.CompareTo(b.Type); });

        return biomes;
    }


    // Sort by Name
    public static List<Biome> SortByName(List<Biome> biomes, bool reverse = false)
    {
        // Z-A
        if (reverse) biomes.Sort((a, b) => { return b.Name.CompareTo(a.Name); });

        // A-Z
        else biomes.Sort((a, b) => { return a.Name.CompareTo(b.Name); });

        return biomes;
    }


    // Sort by Index
    public static List<Biome> SortByIndex(List<Biome> biomes, bool reverse = false)
    {
        // Inf-0
        if (reverse) biomes.Sort((a, b) => { return b.Index.CompareTo(a.Index); });

        // 0-Inf
        else biomes.Sort((a, b) => { return a.Index.CompareTo(b.Index); });

        return biomes;
    }


    // Sort by Value
    public static List<Biome> SortByValue(List<Biome> biomes, bool reverse = false)
    {
        // Inf-0
        if (reverse) biomes.Sort((a, b) => { return b.Value.CompareTo(a.Value); });

        // 0-Inf
        else biomes.Sort((a, b) => { return a.Value.CompareTo(b.Value); });

        return biomes;
    }
    #endregion
}