﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Map;

[CreateAssetMenu(fileName = "Biome Data", menuName = "Data/Biome Data", order = 1)]
public class BiomeData : ScriptableObject
{
    public static BiomeData VOID, PLAINS, FOREST, SWAMP, DESERT, FROZEN, OIL, METAL, STONE, SEA, SKY, SURFACE, NETHER, CRYSTAL, MAGIC;
    public static BiomeData BORDER;

    public BiomeType type;
    public int index;
    public float value;

    [Range(0, 1)]
    public float voidCutoff = 0.4f;        // Threshhold below which biome results in empty space
    public Color color;

    public List<Tile> tiles;
 

    // Sprite data, Background
    // Logic data
    // Liquids, gases, structures
    // Temperature range
    // Diseases
    // MOBs


    // Initialize
    public void Init(bool renameAsset = false)
    {
        this.name = type.ToString();
        this.index = (int)type;
        this.value = index * 0.05f;

        // Normalize tile values
        SetTileValues(this.tiles, this.voidCutoff);

        // Rename Asset
        if (renameAsset)
        {
            string assetPath = AssetDatabase.GetAssetPath(this.GetInstanceID());
            AssetDatabase.RenameAsset(assetPath, this.name);
            AssetDatabase.SaveAssets();
        }
    }


    // Constructor
    public BiomeData(BiomeType type, Color color, List<Tile> terrainTiles, float voidCutoff = 0f)
    {
        this.type = type;
        this.name = type.ToString();
        this.index = (int)type;
        this.color = color;

        this.tiles = terrainTiles;

        this.voidCutoff = voidCutoff;
        this.value = index * 0.05f;

        // Normalize tile values
        SetTileValues(this.tiles, this.voidCutoff);
    }


    public void SetTileValues()
    {
        SetTileValues(this.tiles, this.voidCutoff);
    }


    // Set tile values
    public void SetTileValues(List<Tile> tilesList, float voidCutoff)
    {
        float totalWeight = 0;

        // Calculate total weight
        foreach (Tile tile in tilesList)
        {
            totalWeight += tile.probability;
        }


        // Add void
        totalWeight += voidCutoff;
        float currentWeight = voidCutoff;

        // Calculate value
        for (int i = 0; i < tiles.Count; i++)
        {
            tiles[i].Index = i;

            currentWeight += tiles[i].probability;
            float value = currentWeight / totalWeight;

            tiles[i].Value = Mathf.Clamp01(value);
        }
    }


    // Get Tiles
    public List<Tile> GetTiles()
    {
        return tiles;
    }

    public void SetTiles(List<Tile> tiles)
    {
        this.tiles = tiles;
    }
}