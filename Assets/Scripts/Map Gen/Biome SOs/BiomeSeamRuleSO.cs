﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Biome Seam Rule SO", menuName = "Data/Biome Seam Rule SO", order = 4)]
public class BiomeSeamRuleSO : ScriptableObject
{
    [Header("Boundary")]
    public Vector2 xSpan;
    public Vector2 ySpan;


    public bool markArea, markType;
    public bool seamAsMin, seamAsMax;

    [Header("")]
    public List<BiomeType> biomeTypes;
    public List<BiomeSeam> seamsList;


    // If mark by type, do one method, else do the other
    public Color Apply(int x, int y, BiomeType biomeType, Color defaultColor)
    {
        // Set last to clear
        if (seamsList.Count > 1)
        {
            seamsList[seamsList.Count - 1].debugColor = Color.clear;
            seamsList[seamsList.Count - 1].probability = 100 - MapGenerator.SeamFillRaw * seamsList.Count;
        }

        // Mark variant
        if (markType) defaultColor = ApplyToType(seamsList, biomeTypes, biomeType, defaultColor);
        if (markArea) defaultColor = ApplyToArea(seamsList, xSpan, ySpan, x, y, defaultColor);

        return defaultColor;
    }


    // Apply to Area
    public Color ApplyToArea(List<BiomeSeam> seamsList, Vector2 xSpan, Vector2 ySpan, int x, int y, Color defaultColor)
    {
        if (seamAsMin) ySpan.x = 100 - MapGenerator.SeamsHeightRaw_Percent;
        if (seamAsMax) ySpan.y = 100 - MapGenerator.SeamsHeightRaw_Percent;

        BiomeSeamRule rule = new BiomeSeamRule(seamsList, xSpan, ySpan);
        defaultColor = BiomeSeamRule.Apply(rule, x, y, defaultColor);

        return defaultColor;
    }


    // Apply to Type
    public Color ApplyToType(List<BiomeSeam> seamsList, List<BiomeType> types, BiomeType type, Color defaultColor)
    {
        BiomeSeamRule rule = new BiomeSeamRule(seamsList, types);
        defaultColor = BiomeSeamRule.Apply(rule, type, defaultColor);
        return defaultColor;
    }
}
