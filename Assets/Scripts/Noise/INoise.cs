﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INoise
{
    int Width { get; set; }
    int Height { get; set; }
    float[,] NoiseMap { get; set; }
    Color[] ColorMap { get; set; }
    Texture2D TextureMap { get; set; }
}
