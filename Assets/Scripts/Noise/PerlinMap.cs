﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinMap : INoise
{
    [Header("Map Size")]
    private int width;
    private int height;

    public int Width { 
        get { return width; }
        set { width = value; } 
    }

    public int Height
    {
        get { return height; }
        set { height = value; }
    }


    [Header("Noise Settings")]
    private int seed;
    private float scale;
    [Range(1,8)]
    private int octaves;
    [Range(0,1)]
    private float granularity;
    [Range(1,100)]
    private float gapSize;
    Vector2 offset;


    [Header("Perlin Maps")]
    private float[,] noiseMap;
    private Color[] colorMap;
    private Texture2D textureMap;


    // Noise Map
    public float[,] NoiseMap
    {
        get { return noiseMap; }
        set { noiseMap = value; }
    }

    // Color Map
    public Color[] ColorMap
    {
        get { return colorMap; }
        set { colorMap = value; }
    }

    // Texture Map
    public Texture2D TextureMap
    {
        get { return textureMap; }
        set { textureMap = value; }
    }


    // ----------------- MAKE STUFF FOR TILEMAP AS WELL??? ----------------
    // --------------------------------------------------------------------
    // --------------------------------------------------------------------



    // Constructor
    public PerlinMap(int width, int height, int seed = 0, float scale = 1, int octaves = 1, float granularity = 0.5f, float gapSize = 1, Vector2 offset = default)
    {
        this.noiseMap = PerlinNoise.Create(width, height, seed, scale, octaves, granularity, gapSize, offset);
        this.colorMap = NoiseUtils.SetColors(noiseMap);
        this.textureMap = NoiseUtils.CreateTexture(width, height, noiseMap);
        // this.tilemap = PerlinNoise.GeneratePerlinTilemap(width, height, colorMap, tilemap, blankTile);
    }


    // Create Texture 2D
    public static Texture2D Create(int width, int height, int seed = 0, float scale = 1, int octaves = 1, float granularity = 0.5f, float gapSize = 1, Vector2 offset = default)
    {
        return new PerlinMap(width, height, seed, scale, octaves, granularity, gapSize, offset).textureMap;
    }
}
