﻿/*
    ------------------ Voronoi Noise -------------------
 
    Voronoi noise seeds an area with a number of center points
    and then creates regions based on the shortest distance to
    each of those points.

    Useful for creating biomes.  Can create different rules
    depending on depth.

    Create the following parameters on the Game Object you wish
    to have generate Voronoi Noise:

    - Width (int, > 0)
    - Height (int, > 0)
    - Seed (int)
    - NumberOfSeeds (int, Range(1,1024))
    - CentroidOffset (int, Range(1,64))
    - Map (Tilemap)
    - Tiles (TileBase[])

    Can be combined with Perlin Noise for greater effect by
    applying a z-value based on the height map data:

    https://imgur.com/gallery/8OfWc

    https://www.youtube.com/watch?v=1qxytunVuqQ

    ----------------------------------------------------
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VoronoiNoise
{
    public const float CULLING_VALUE = 0f;

    static List<Vector3Int> centroids;                  // Seed positions
    static List<Vector3Int> centroidsToBeCulled;        // Flagged seeds
    static List<float> heightMapList;                   // Store height values


    // Create random seeds
    public static void CreateRandomSeeds(int width, int height, int numberOfCentroids, float[,] noiseMap, float[] valuesArray)
    {
        centroids = new List<Vector3Int>();
        centroidsToBeCulled = new List<Vector3Int>();
        heightMapList = new List<float>();

        // Loop through all cells
        for (int i = 0; i < numberOfCentroids; i++)
        {
            // Get random cell
            int x = Random.Range(0, width - 1);
            int y = Random.Range(0, height - 1);
            
            centroids.Add(new Vector3Int(x, y, 0));

            // Get random value
            int index = Random.Range(0, valuesArray.Length);
            heightMapList.Add(valuesArray[index]);

            // Assign value to cell
            noiseMap[x, y] = valuesArray[index];
        }
    }
    

    // Create Seeds
    public static void CreateSeeds(int width, int height, int numCentroids, float[,] noiseMap, float[] valuesArray, int seed = 0, int voronoiSeedOffset = 0)
    {
        // Seed
        System.Random prng = new System.Random(seed);

        centroids = new List<Vector3Int>();
        heightMapList = new List<float>();


        // Calculate rows and columns
        int rows;
        int columns;

        rows = Mathf.FloorToInt(Mathf.Sqrt(numCentroids));
        while (numCentroids % rows != 0)
            rows -= 1;
        rows = Mathf.Clamp(rows, 1, height);

        columns = Mathf.RoundToInt(numCentroids / rows);
        columns = Mathf.Clamp(columns, 1, width);


        // Loop through all cells
        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                // Get pseudo-random cell
                Vector3Int offset = new Vector3Int(0, 0, 0);

                offset.x = prng.Next(-voronoiSeedOffset, voronoiSeedOffset);
                offset.y = prng.Next(-voronoiSeedOffset, voronoiSeedOffset);

                int posX = x * width / columns + offset.x;
                int posY = y * height / rows + offset.y;

                posX = Mathf.Clamp(posX, 0, width);
                posY = Mathf.Clamp(posY, 0, height);

                Vector3Int centroid = new Vector3Int(posX, posY, 0);
                centroids.Add(centroid);


                // Get pseudo-random value
                int index = prng.Next(0, valuesArray.Length);


                // Assign value to cell
                heightMapList.Add(valuesArray[index]);
                noiseMap[x, y] = valuesArray[index];
            }
        }
    }


    // Create Seeds
    public static void CreateRuledSeeds(int width, int height, int numCentroids, float[,] noiseMap, float[] valuesArray, int seed = 0, int voronoiSeedOffset = 0)
    {
        // Seed
        //System.Random prng = new System.Random(seed);
        System.Random prng = MapGenerator.PRNG;

        centroids = new List<Vector3Int>();
        heightMapList = new List<float>();


        // Calculate rows and columns
        int rows;
        int columns;

        rows = Mathf.FloorToInt(Mathf.Sqrt(numCentroids));
        while (numCentroids % rows != 0)
            rows -= 1;
        rows = Mathf.Clamp(rows, 1, height);

        columns = Mathf.RoundToInt(numCentroids / rows);
        columns = Mathf.Clamp(columns, 1, width);


        // Loop through all cells
        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                // Get pseudo-random cell
                Vector3Int offset = new Vector3Int(0, 0, 0);

                offset.x = prng.Next(-voronoiSeedOffset, voronoiSeedOffset);
                offset.y = prng.Next(-voronoiSeedOffset, voronoiSeedOffset);

                int posX = x * width / columns + offset.x;
                int posY = y * height / rows + offset.y;

                posX = Mathf.Clamp(posX, 0, width);
                posY = Mathf.Clamp(posY, 0, height);

                Vector3Int centroid = new Vector3Int(posX, posY, 0);
                centroids.Add(centroid);


                #region BIOME RULES
                // DEFAULT RULE - Get pseudo-random value
                int index = prng.Next(0, valuesArray.Length);

                // Assign value to cell
                float value = BiomeManager.ApplyCurrentRules(x, y, width, height, centroid, index, valuesArray);

                heightMapList.Add(value);
                noiseMap[x, y] = value;

                #endregion
            }
        }
    }


    // Return closest cell index
    private static int FindClosestCell(Vector3Int cell)
    {
        // Distance to first centroid
        int closestCellIndex = 0;
        var distance = Vector3Int.Distance(cell, centroids[0]);

        // Compare distance to each centroid
        for (int i = 0; i < centroids.Count; i++)
        {
            var tempDistance = Vector3Int.Distance(cell, centroids[i]);
            if (tempDistance < distance)
            {
                distance = tempDistance;
                closestCellIndex = i;
            }
        }

        return closestCellIndex;
    }


    // Generate Noise Map
    public static float[,] Create(int width, int height, int numCentroids, float[] valuesArray, int cullingHeight, int seed = 0, int voronoiSeedOffset = 0)
    {
        float[,] noiseMap = new float[width, height];
        numCentroids = Mathf.Clamp(numCentroids, 1, width * height);

        // Create Seeds
        // CreateRandomSeeds(width, height, numCentroids, noiseMap, valuesArray)
        //CreateSeeds(width, height, numCentroids, noiseMap, valuesArray, seed, voronoiSeedOffset);
        CreateRuledSeeds(width, height, numCentroids, noiseMap, valuesArray, seed, voronoiSeedOffset);


        // Special Rules
        ////FlagSeedsForDeletion(cullingHeight, noiseMap);
        

        // Loop through all cells
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                float value = Biome.BORDER.Value;

                // Check if in bounds
                if (NoiseUtils.InBounds(x, y, MapGenerator.InnerBorderSize))
                {
                    // Find closest cell
                    int closestCellIndex = FindClosestCell(new Vector3Int(x, y, 0));
                    value = heightMapList[closestCellIndex];

                    if (centroids[closestCellIndex].y > MapGenerator.SkyHeight || y > MapGenerator.SheerHeight)
                        value = 0;
                }

                if (!NoiseUtils.InBounds(x, y, MapGenerator.BorderSize))
                { 
                    value = Biome.BORDER.Value; 
                }

                // Set value
                noiseMap[x, y] = value;     // Same value as value of closest centroid
            }
        }

        return noiseMap;
    }
}
