﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoronoiMap : INoise
{
    [Header("Map Size")]
    private int width;
    private int height;

    public int Width
    {
        get { return width; }
        set { width = value; }
    }

    public int Height
    {
        get { return height; }
        set { height = value; }
    }

    private float[] valuesArray;


    [Header("Noise Settings")]
    private int numCentroids;

    private float[,] noiseMap;
    private Color[] colorMap;
    private Texture2D textureMap;

    // Noise Map
    public float[,] NoiseMap
    {
        get { return noiseMap; }
        set { noiseMap = value; }
    }

    // Color Map
    public Color[] ColorMap
    {
        get { return colorMap; }
        set { colorMap = value; }
    }

    // Texture Map
    public Texture2D TextureMap
    {
        get { return textureMap; }
        set { textureMap = value; }
    }


    // Constructor
    public VoronoiMap(int width, int height, int numCentroids, float[] valuesArray = null, int cullingHeight = 0, int seed = 0, int voronoiSeedOffset = 0)
    {
        this.noiseMap = VoronoiNoise.Create(width, height, numCentroids, valuesArray, cullingHeight, seed, voronoiSeedOffset);
        this.colorMap = NoiseUtils.SetColors(noiseMap);
        this.textureMap = NoiseUtils.CreateTexture(width, height, noiseMap);
    }


    // Create Texture 2D
    public static Texture2D Create(int width, int height, int numCentroids, float[] valuesArray = null, int cullingHeight = 0, int seed = 0, int voronoiSeedOffset = 0)
    {
        return new VoronoiMap(width, height, numCentroids, valuesArray, cullingHeight, seed, voronoiSeedOffset).textureMap;
    }
}
