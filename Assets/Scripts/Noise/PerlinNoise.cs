﻿/*
    ------------------ Perlin Noise -------------------
 
    A simple script for generating heightmap data.  Attach
    this to a quad or other object with a mesh renderer and
    an Unlit shader to display the texture information.

    Can be used in combination with Voronoi Noise to generate
    more complex data, such as biomes.

    Create the following parameters on the Game Object you wish
    to have generate Perlin Noise:

    - Width (int, > 0)
    - Height (int, > 0)
    - Seed (int)
    - Scale (float)
    - Offset (Vector2)
    - Octaves (int, Range(1,8))
    - Persistence (float, Range(0,1))
    - Lacunarity (float, > 0)
    

    If using a texture, you will want to get the Mesh Renderer
    component before calling the method.  If using a tilemap,
    you will need to know the tilemap and create a blank tile
    with an all-white sprite to be colored.

    https://www.youtube.com/watch?v=bG0uEXV6aHQ

    ----------------------------------------------------
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public static class PerlinNoise
{
    /// <summary>
    /// Generate perlin noise map.
    /// </summary>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="seed"></param>
    /// <param name="scale"></param>
    /// <param name="octaves">Range from 1-8.  Number of times to cycle through the noise map.</param>
    /// <param name="persistence">Range from 0 - 1.  Decreases the amplitude after each octave, thus adding more granularity to the height.</param>
    /// <param name="lacunarity">Value greater than 1.  Increases frequency after each octave.  Higher lacunarity results in larger gaps, thus the higher the frequency, the further apart sample points will be, meaning that height values will change more rapidly.</param>
    /// <param name="offset"></param>
    /// <returns>float[,] noiseMap</returns>
    public static float[,] Create(int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistence, float lacunarity, Vector2 offset)
    {
        float[,] noiseMap = new float[mapWidth, mapHeight];

        // Seed
        System.Random prng = new System.Random(seed);

        // Offset
        Vector2[] octaveOffsets = new Vector2[octaves];
        for (int i = 0; i < octaves; i++)
        {
            float offsetX = prng.Next(-100000, 100000) + offset.x;
            float offsetY = prng.Next(-100000, 100000) + offset.y;

            octaveOffsets[i] = new Vector2(offsetX, offsetY);
        }


        // Clamp values to avoid zero
        if (scale <= 0)
            scale = 0.0001f;
        if (mapWidth < 1)
            mapWidth = 1;
        if (mapHeight < 1)
            mapHeight = 1;
        if (lacunarity < 1)
            lacunarity = 1;
        if (octaves < 0)
            octaves = 0;


        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;

        float halfWidth = mapWidth * 0.5f;
        float halfHeight = mapHeight * 0.5f;


        // Loop through coordinates
        for (int x = 0; x < mapWidth; x++)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                float amplitude = 1;
                float frequency = 1;
                float noiseHeight = 0;


                // Octaves
                for (int i = 0; i < octaves; i++)
                {
                    // Convert to decimal percent
                    float xCoord = (float)(x - halfWidth) / scale * frequency + octaveOffsets[i].x;
                    float yCoord = (float)(y - halfHeight) / scale * frequency + octaveOffsets[i].y;

                    float perlinValue = Mathf.PerlinNoise(xCoord, yCoord) * 2 - 1;
                    // noiseMap[x, y] = perlinValue;
                    noiseHeight += perlinValue * amplitude;

                    amplitude *= persistence;
                    frequency *= lacunarity;
                }

                // Establish parameters
                if (noiseHeight > maxNoiseHeight)
                {
                    maxNoiseHeight = noiseHeight;
                }
                else if (noiseHeight < minNoiseHeight)
                {
                    minNoiseHeight = noiseHeight;
                }


                // Apply the height value to the coordinate
                noiseMap[x, y] = noiseHeight;
            }
        }

        // Normalize value to 0-1
        for (int x = 0; x < mapWidth; x++)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                noiseMap[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap[x, y]);
            }
        }

        return noiseMap;
    }


    /// <summary>
    /// Automatically generate a Perlin Noise texture to be applied to a mesh, such as a plane, quad, or other mesh.
    /// </summary>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="seed"></param>
    /// <param name="scale"></param>
    /// <param name="octaves">Range from 1-8.  Number of times to cycle through the noise map.</param>
    /// <param name="persistence">Range from 0 - 1.  Decreases the amplitude after each octave, thus adding more granularity to the height.</param>
    /// <param name="lacunarity">Value greater than 1.  Increases frequency after each octave.  Higher lacunarity results in larger gaps, thus the higher the frequency, the further apart sample points will be, meaning that height values will change more rapidly.</param>
    /// <param name="offset"></param>
    /// <param name="renderer">The mesh renderer to apply the texture to.  Recommended using an unlit shader for the material.</param>
    /// <param name="viewInEditMode">Whether the noise map will update in Editor Mode or not.</param>
    public static Texture2D AutoGeneratePerlinTexture(int width, int height, int seed, float scale, int octaves, float persistence, float lacunarity, Vector2 offset, Renderer renderer, bool viewInEditMode)
    {
        float[,] noiseMap = Create(width, height, seed, scale, octaves, persistence, lacunarity, offset);
        
        Texture2D texture = NoiseUtils.CreateTexture(width, height, noiseMap);
        NoiseUtils.ApplyTexture(texture, renderer, viewInEditMode);

        return texture;
    }


    /// <summary>
    /// Automatically generate Perlin Noise to be applied to a tilemap.
    /// </summary>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="seed"></param>
    /// <param name="scale"></param>
    /// <param name="octaves">Range from 1-8.  Number of times to cycle through the noise map.</param>
    /// <param name="persistence">Range from 0 - 1.  Decreases the amplitude after each octave, thus adding more granularity to the height.</param>
    /// <param name="lacunarity">Value greater than 1.  Increases frequency after each octave.  Higher lacunarity results in larger gaps, thus the higher the frequency, the further apart sample points will be, meaning that height values will change more rapidly.</param>
    /// <param name="offset"></param>
    /// <param name="tilemap">The tilemap upon which the Perlin Noise will be generated.</param>
    /// <param name="blankTile">Blank white tile that serves as a base.  Does not need a collider.</param>

    public static void AutoGeneratePerlinTilemap (int width, int height, int seed, float scale, int octaves, float persistence, float lacunarity, Vector2 offset, Tilemap tilemap, TileBase blankTile)
    {
        float[,] noiseMap = Create(width, height, seed, scale, octaves, persistence, lacunarity, offset);
        NoiseUtils.SetTilemap(width, height, noiseMap, tilemap, blankTile);
    }
}
