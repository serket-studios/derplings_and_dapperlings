﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public static class NoiseUtils
{
    /// <summary>
    /// Create black and white color map for noise.
    /// Can then be applied to a texture or tile sprite.
    /// </summary>
    /// <param name="noiseMap">Noise map from the GenerateNoiseMap() method.</param>
    /// <returns></returns>
    public static Color[] SetColors(float[,] noiseMap)
    {
        int width = noiseMap.GetLength(0);
        int height = noiseMap.GetLength(1);

        Color[] colorMap = new Color[width * height];

        // Loop through coordinates
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                colorMap[y * width + x] = Color.Lerp(Color.black, Color.white, noiseMap[x, y]);
            }
        }

        return colorMap;
    }


    /// <summary>
    /// Create black and white color map for noise.
    /// </summary>
    /// <param name="texture"></param>
    /// <returns></returns>
    public static Color[] GetColors(Texture2D texture)
    {
        return texture.GetPixels();
    }


    /// <summary>
    /// Create a noise texture to be applied to the shared material of a mesh renderer,
    /// such as a plane or quad.
    /// </summary>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="colorMap">Black and white color map from the CalculatePerlinColors() method.</param>
    /// <returns></returns>
    public static Texture2D CreateTexture(int width, int height, Color[] colorMap)
    {
        Texture2D texture = new Texture2D(width, height);

        texture.SetPixels(colorMap);
        texture.Apply();
        return texture;
    }


    /// <summary>
    /// Generate texture map based on noise map.
    /// </summary>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="noiseMap"></param>
    /// <returns></returns>
    public static Texture2D CreateTexture(int width, int height, float[,] noiseMap)
    {
        return CreateTexture(width, height, SetColors(noiseMap));
    }


    // Apply Texture
    public static void ApplyTexture(Texture2D texture, Renderer renderer, bool viewInEditMode = false)
    {
        if (viewInEditMode)
            renderer.sharedMaterial.mainTexture = texture;

        else
            renderer.material.mainTexture = texture;
    }


    // In Bounds
    public static bool InBounds(int x, int y, int width, int height, Vector2Int borderSize)
    {
        return (x > borderSize.x && x < width - borderSize.x && y > borderSize.y && y < height - borderSize.y);
    }

    public static bool InBounds(int x, int y, Vector2Int borderSize)
    {
        return InBounds(x, y, MapGenerator.Width, MapGenerator.Height, borderSize);
    }

    public static bool InBounds(int x, int y)
    {
        return InBounds(x, y, MapGenerator.BorderSize);
    }


    // In Bounds Size
    public static Vector2Int InBoundSize(Vector2Int borderSize)
    {
        return new Vector2Int(MapGenerator.Width - borderSize.x, MapGenerator.Height - borderSize.x);
    }

    public static Vector2Int InBoundWidth(Vector2Int borderSize)
    {
        return new Vector2Int(borderSize.x, MapGenerator.Width - borderSize.x);
    }

    public static Vector2Int InBoundHeight(Vector2Int borderSize)
    {
        return new Vector2Int(borderSize.y, MapGenerator.Height - borderSize.y);
    }






    /// <summary>
    /// Create a noise texture to be applied to tilemap.  
    /// Requires setting up a blank white tile to act as a base.
    /// </summary>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="colorMap">Black and white color map from the CalculatePerlinColors() method.</param>
    /// <param name="tilemap">The tilemap upon which the Perlin Noise will be generated.</param>
    /// <param name="blankTile">Blank white tile that serves as a base.  Does not need a collider.</param>
    public static void SetTilemap(int width, int height, Color[] colorMap, Tilemap tilemap, TileBase blankTile)
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                // Set tiles
                Vector3Int pos = new Vector3Int(x, y, 0);
                tilemap.SetTile(pos, blankTile);

                // Set colors
                Color color = colorMap[y * width + x];
                tilemap.SetTileFlags(pos, TileFlags.None);
                tilemap.SetColor(pos, color);
            }
        }
    }


    public static void SetTilemap(int width, int height, float[,] noiseMap, Tilemap tilemap, TileBase blankTile)
    {
        SetTilemap(width, height, SetColors(noiseMap), tilemap, blankTile);
    }


    /// ----------------------------------------------------------------------------------------------------------------------------------------
    ///                                                           GET && SET
    ///                                                      (Value - Color - Pixel)
    /// ----------------------------------------------------------------------------------------------------------------------------------------
    #region
    // Get Cell Value
    public static float GetCellValue(int x, int y, INoise noise)
    {
        return noise.NoiseMap[x, y];
    }


    // Set Cell Value
    public static void SetCellValue(int x, int y, float value, INoise noise)
    {
        noise.NoiseMap[x, y] = value;
    }


    // Get Cell Color
    public static Color GetCellColor(int x, int y, INoise noise)
    {
        return noise.ColorMap[y * noise.Width + x];
    }


    // Set Cell Color
    public static void SetCellColor(int x, int y, Color color, INoise noise)
    {
        noise.ColorMap[y * noise.Width + x] = color;
    }


    // Get Cell Color
    public static Color GetCellColorFromTexture(int x, int y, INoise noise)
    {
        return noise.TextureMap.GetPixel(x, y);
    }


    // Set Cell Color
    public static void SetCellColorOnTexture(int x, int y, Color color, INoise noise)
    {
        noise.TextureMap.SetPixel(x, y, color);
    }
    #endregion
}
