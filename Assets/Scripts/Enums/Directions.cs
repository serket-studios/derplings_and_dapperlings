﻿/*
    ----------------- Directions -------------------
 
    Common uses of directions.  Used in conjuntion
    with the DirectionUtils script to set Vectors.

    Call "using Directions" namespace on other classes
    in order to reference to these lists.

    - Directions
    - Compass Directions (Cardinal, Intermediate, Normalized)
    - Movement Directions

    -------------------------------------------------
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Directions
{
    /// <summary>
    /// Up, down, left, right, top, bottom
    /// </summary>
    public enum Direction
    {
        Up, Down, Left, Right, Top, Bottom
    }


    /// <summary>
    /// NSEW, NE, NW, NE, NW
    /// Normalized versions of intermediary
    /// </summary>
    public enum CompassDirection
    {
        North, South, East, West,
        Northeast, Northwest, Southeast, Southwest,
        NE_Normalized, NW_Normalized, SE_Normalized, SW_Normalized
    }


    /// <summary>
    /// Used for movement left to right, right to left, top to bottom, bottom to top.
    /// </summary>
    public enum MoveDirection
    {
        LeftToRight, RightToLeft, TopToBottom, BottomToTop
    }
}
