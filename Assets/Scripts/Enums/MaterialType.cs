﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MaterialType
{
    NONE,
    METAL,
    ROCK, 
    ORGANIC, 
    LIQUIFIABLE, 
    GEM,
    OTHER
}
