﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StateOfMatter
{
    SOLID, LIQUID, GAS, PLASMA, VOID
}
