﻿public enum TerrainType
{
    Adamantine, Aluminium, Amber, Amethyst, Amphibolite, Amygdalite,
    Basalt, Bismuth, Bloodstone, Brimstone,
    Clay, Coal, Copper, Diamond, Dirt, Emerald,
    Feldspar, Firestone, Fossil, Glowstone, Gold, Granite,
    Ice, Iron, Jade, Lead, Limestone,
    Magicite, Marble, Mud, Mythril, Netherak,
    Obsidian, Quartz, Ruby,
    Sand, Sandstone, Sapphire, Scale, Shale, Silver, Slate, Slime, Snow, Steel,
    Titanium, Tungsten,
    Void
}
